#include <iostream>

#include <foo.hpp>

int main()
{
    Bar bar;

    std::cout << "Hello World! - " << bar.baz() << std::endl;
    
    return 0;
}
